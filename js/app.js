import { API } from './api.js'
import * as UI from './interfaz.js';

UI.formularioBuscar.addEventListener('submit', (e) => {
     e.preventDefault();

     // obtener datos del formulario
     const artista = document.querySelector('#artista').value,
          cancion = document.querySelector('#cancion').value;

     if (artista === '' || cancion === '') {
          // El usuario deja los campos vacios, marca error
          mostrarMensajes(
               'Error... Todos los campos son obligatorios',
               'error'
          );
     } else {
          // El formulario esta completo, realizar consulta a la API
          const api = new API(artista, cancion);
          api.consultarApi()
               .then(data => {
                    if (data.respuesta.lyrics) {
                         // la cancion existe
                         const letra = data.respuesta.lyrics;
                         UI.divResultado.textContent = letra;
                    } else {
                         // la cancion no existe
                         UI.divResultado.textContent = '';
                         mostrarMensajes(
                              'La cancion NO existe, prueba con otra busqueda',
                              'error'
                         );
                         UI.formularioBuscar.reset();
                    }
               });
     }
});

// pintar todos los mensjaes de error
function mostrarMensajes(mensaje, clase) 
{
     UI.divMensajes.innerHTML = mensaje;
     UI.divMensajes.classList.add(clase);
     setTimeout(() => {
          UI.divMensajes.innerHTML = '';
          UI.divMensajes.classList.remove('error');
     }, 3000);
}